# Front-end Project Template

This project was developed to be used as a template for further development.

## Getting Started

These instructions will get your copy of this project up and running on your local machine for development and/or testing purposes.

### Prerequisites

[Node.js](https://nodejs.org/en/download/) must be installed.

### Installing

Open a command prompt/command line window and navigate to project directory.
```
cd \downloads\react-template-dev
```

In the project main directory, run the following command to install project dev dependencies:
```
npm install
```

Once the installation is finished, run the following grunt tasks to build the project resources into the dist folder and start the http server:
```
npm start
```

The browser should open automatically on the index page.

## Authors

* **Hisrael Anjo Pereira Junior** - [LinkedIn](https://www.linkedin.com/in/hisrael/) / [GitHub](https://github.com/hisrael)

## Built With

* [React.js](https://facebook.github.io/react/) - 15.6.1
* [Redux.js](http://redux.js.org/) - 3.7.2
* [jQuery](https://jquery.com/) - 3.2.1
* [Bootstrap](http://getbootstrap.com/) - 3.3.7

## License

This project is licensed under the MIT License.
