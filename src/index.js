import React from 'react';
import ReactDOM from 'react-dom';
import App from './scripts/App';

import 'jquery/src/jquery';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.css'

import './styles/main.css';

ReactDOM.render(<App />, document.getElementById('root'));
